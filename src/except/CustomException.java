package except;

@SuppressWarnings("serial")
public class CustomException extends Exception {
	String msg;

	public CustomException(String msg) {
		super(msg);
		this.msg = msg;
	}


}
